## CONFIGURE NGINX
FROM nginx:1.15.5
COPY --from=0 /build/public /usr/share/nginx/html
RUN chown -R nginx:nginx /var/cache/nginx && \
        chown -R nginx:nginx /var/log/nginx && \
        chown -R nginx:nginx /etc/nginx/conf.d
RUN touch /var/run/nginx.pid && \
        chown -R nginx:nginx /var/run/nginx.pid
EXPOSE 80